package ctrl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import org.apache.log4j.Logger;

import model.question.Question;
import model.question.Question1;
import model.question.Question10;
import model.question.Question11;
import model.question.Question12;
import model.question.Question13;
import model.question.Question14;
import model.question.Question15;
import model.question.Question16;
import model.question.Question17;
import model.question.Question2;
import model.question.Question3;
import model.question.Question4;
import model.question.Question5;
import model.question.Question6;
import model.question.Question7;
import model.question.Question8;
import model.question.Question9;
import model.rules.Rule;
import model.rules.Rule1;
import model.rules.Rule10;
import model.rules.Rule2;
import model.rules.Rule3;
import model.rules.Rule4;
import model.rules.Rule5;
import model.rules.Rule6;
import model.rules.Rule7;
import model.rules.Rule8;
import model.rules.Rule9;

public class MainCtrl {
	final static Logger logger = Logger.getLogger(MainCtrl.class);
	private DecimalFormat decimalFormatTwoTwo = new DecimalFormat("##.##");
	
	ArrayList<Integer> authorIds;
	private ArrayList<Question> questions = new ArrayList<Question>();
	private ArrayList<Rule> rules = new ArrayList<Rule>();
	
	private int cntThreads = 0;
	private Semaphore sem = new Semaphore(1);
	private Semaphore semCnt = new Semaphore(1);
	private Semaphore semWaitForThreads = new Semaphore(1);

	public static void main(String[] args) {
		try {
			MainCtrl mainCtrl = new MainCtrl();
			mainCtrl.doIt();
		} catch (InterruptedException ie) {
			logger.error(ie);
		}
		
	}
	
	public void doIt() throws InterruptedException  {
		long durationStart = System.currentTimeMillis();
		
		Thread thread = null;
		sem.acquire();
		
		// author_posts initialisieren
		DbCtrl.getInstance().initAuthorPosts();
		
		
		// Question-Instanzen erzeugen
		this.generateQuestions();
		cntThreads = questions.size();
		
		
		// Question Threads erzeugen und starten
		for (Question question : questions) {
			semWaitForThreads.acquire();
			
			thread = new Thread(question);
			thread.start();
			
			logger.info("Thread for question " + question.getId() + " started: '" + question.getDesc() + "'");
		}
		
		sem.acquire();
		
		// TODO: Rule-Instanzen erzeugen
		this.generateRules();
		cntThreads = rules.size();
		
		
		// TODO: Rule Threads erzeugen und starten
		for (Rule rule : rules) {
			thread = new Thread(rule);
			thread.start();
			
			logger.info("Thread for rule " + rule.getId() + " started");
		}
		
		sem.acquire();
		
//		// TODO: Auswertung, Ausgabe, Speichern??
//		for (Integer author : authorIds) {
//			if (author.getPoints() != 0) {
//				System.out.println("a" + author.getId() + ":p" + author.getPoints());
//			}
//		}
		
		// FERTIG
		DbCtrl.getInstance().commit();
		logger.info("Duration: " + decimalFormatTwoTwo.format((System.currentTimeMillis() - durationStart)*(2.78*Math.pow(10, -7))) + "h.");
	}
	
	public void generateQuestions() {
		questions.add(new Question1(this));
		questions.add(new Question2(this));
		questions.add(new Question3(this));
		questions.add(new Question4(this));
		questions.add(new Question5(this));
		questions.add(new Question6(this));
		questions.add(new Question7(this));
		questions.add(new Question8(this));
		questions.add(new Question9(this));
		questions.add(new Question10(this));
		questions.add(new Question11(this));
		questions.add(new Question12(this));
		questions.add(new Question13(this));
		questions.add(new Question14(this));
		questions.add(new Question15(this));
		questions.add(new Question16(this));
		questions.add(new Question17(this));
	}
	
	public void generateRules() {
		rules.add(new Rule1(this));
		rules.add(new Rule2(this));
		rules.add(new Rule3(this));
		rules.add(new Rule4(this));
		rules.add(new Rule5(this));
		rules.add(new Rule6(this));
		rules.add(new Rule7(this));
		rules.add(new Rule8(this));
		rules.add(new Rule9(this));
		rules.add(new Rule10(this));
	}
	
	public synchronized void threadEnded(int threadId) {
		logger.info("T" + threadId + " ended. " + (cntThreads-1) + " have to end.");
		
		try {
			// Variablen sperren
			semCnt.acquire();
			
			// Gesamtanzahl Threads
			--cntThreads;
			
			if (cntThreads == 0) {
				sem.release();
			}
			
			// Naechsten Thread starten
			semWaitForThreads.release();
			
			// Variablen freigeben
			semCnt.release();
		} catch (InterruptedException e) {
			logger.error(e);
		}
	}

}
