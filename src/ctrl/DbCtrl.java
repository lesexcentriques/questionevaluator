package ctrl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

public class DbCtrl {
	private static volatile DbCtrl instance;
	final static Logger logger = Logger.getLogger(DbCtrl.class);
	
	private final String PSQL_USER = "postgres";
	private final String PSQL_PWD = "iHC3QZ2zKQx1b2QW21hq";
	private final String HOST = "jdbc:postgresql://localhost:5432/datatextmining";
	
	private Connection connection = null;
	
	private DbCtrl() { };
	
	public static DbCtrl getInstance() {
		if (instance == null) {
			synchronized (DbCtrl.class) {
				if (instance == null) {
					instance = new DbCtrl();
				}
			}
		}
		
		return instance;
	}
	
	private void connect() {
		if (connection == null) {
			try {
				// Datenbankverbindung herstellen
//				Class.forName("org.postgresql.Driver");
				DriverManager.registerDriver(new org.postgresql.Driver());
				connection = DriverManager.getConnection(HOST, PSQL_USER, PSQL_PWD);
				connection.setAutoCommit(false); //AutoCommit deaktivieren
				
				//result.setAutoCommit(false); //AutoCommit deaktivieren
			} catch (Exception e) {
				logger.error(e.toString());
				logger.error(e.getMessage());
			}
		} else {
			try {
				if (connection.isClosed()) {
					connection = DriverManager.getConnection(HOST, PSQL_USER, PSQL_PWD);
				}
			} catch (SQLException e) {
				logger.error(e.toString());
				logger.error(e.getMessage());
			}
		}
	}
	
	/**
	 * Committet die Aenderungen.
	 */
	public void commit(){
		try {
			connection.commit();
		} catch (SQLException e) {
			logger.error("commit: "+e.getMessage());
		}
	}
	
	/**
	 * Macht einen Rollback der Aenderungen.
	 */
	public void rollback(){
		try {
			connection.rollback();
		} catch (SQLException e) {
			logger.error("rollback: "+e.getMessage());
		}
	}
	
	public void close(){
		try {
			connection.close();
		} catch (SQLException e) {		
			e.printStackTrace();
		}
	}
	
	// Allgemein
	public void initAuthorPosts() {
		Statement statement = null;
		String sql = "UPDATE author_points "
				+ "SET q1=0, q2=0, q3=0, q4=0, q5=0, q6=0, q7=0, q8=0, q9=0, q10=0, "
				+ "q11=0, q12=0, q13=0, q14=0, q15=0, q16=0, q17=0, "
				+ "r1=0, r2=0, r3=0, r4=0, r5=0, r6=0, r7=0, r8=0, r9=0, r10=0";
		
		this.connect();
		
		try {
			statement = connection.createStatement();
			statement.execute(sql);
			this.commit();
			logger.info("author_posts initialized.");
		} catch (SQLException sqle) {
			logger.error(sqle);
		}
	}
	
	public ResultSet select(String sql) {
		Statement statement = null;
		ResultSet results = null;
		
		this.connect();
		
		try {
			statement = connection.createStatement();
			results = statement.executeQuery(sql);
		} catch (SQLException sqle) {
			logger.error(sqle);
		}
		
		return results;
	}
	
	public boolean saveToDb(int authorId, String trashName, int points){
		this.connect();
		
		try {
//			PreparedStatement st = connection.prepareStatement("INSERT INTO "
//			    + "author_points(author, "+trashName+") "             
//				+ " VALUES (?, ?) "
//				+ "ON CONFLICT (author) DO UPDATE SET "+trashName+" = EXCLUDED."+trashName);
//			
//			st.setInt(1, authorId);
//			st.setInt(2, points);
			
			PreparedStatement st = connection.prepareStatement("UPDATE author_points "
					+ "SET " + trashName + "=" + points + " "
					+ "WHERE author=" + authorId);
			
			//logger.debug(st.toString());
			
			st.executeUpdate(); // Insert ausfuehren
			st.close();
			
		} catch (SQLException e) {
			logger.error("saveToDb(): "+e.getMessage());
			return false;
		}
		return true;
	}

	public ArrayList<Integer[]> getAuthorIdsFromQuestion(int xq, int yq) {
		ArrayList<Integer[]> result = new ArrayList<>();
		Statement statement = null;
		String sql = "SELECT author, q"+xq+", q"+yq+" FROM author_points"; // TODO: LIMIT entfernen

		// Datenbankverbindung herstellen
		this.connect();

		try {
			// Daten abfragen
			statement = connection.createStatement();
			ResultSet sqlResults = statement.executeQuery(sql);

			// Daten verarbeiten
			while (sqlResults.next()) {
				result.add(new Integer[] {sqlResults.getInt("author"),sqlResults.getInt("q"+xq),sqlResults.getInt("q"+yq)});
			}
		} catch (SQLException e) {
			logger.error(e.toString());
			logger.error(e.getMessage());
		}
		
		return result;		
	}
	
}
