package model.rules;

import java.util.List;

import ctrl.DbCtrl;
import ctrl.MainCtrl;

public abstract class Rule implements Runnable {
	protected int id;	
	private String columnName;
	protected DbCtrl con;
	protected List<Integer[]> authorId;
	private int points;
	protected MainCtrl mainCtrl;

	// KONSTRUKTOR
	public Rule(int id, String columnName, int points , int xq, int yq, MainCtrl mainCtrl) {
		super();
		this.id = id;
		this.columnName = columnName;
		this.points = points;
		this.con = DbCtrl.getInstance();
		this.authorId = con.getAuthorIdsFromQuestion(xq, yq);
		this.mainCtrl = mainCtrl;
	}

	// GETTER UND SETTER
	public int getId() {
		return this.id;
	}
	
	public String getColumnName() {
		return columnName;
	}
	
	public int getPoints(){
		return points;
	}
	
}
