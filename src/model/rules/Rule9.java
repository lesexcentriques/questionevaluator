package model.rules;

import ctrl.MainCtrl;

public class Rule9 extends Rule {

	public Rule9(MainCtrl mainCtrl) {
		super(9, "r9", 30, 7, 16, mainCtrl);
	}
	
	@Override
	public void run() {
		for(Integer[] data : authorId){
			if(data[1] != 0 && data[2] != 0){
				con.saveToDb(data[0],getColumnName(), getPoints());
			}
		}
		
		this.mainCtrl.threadEnded(this.id);
	}

}
