package model.rules;

import ctrl.MainCtrl;

public class Rule6 extends Rule {

	public Rule6(MainCtrl mainCtrl) {
		super(6, "r6", 50, 7, 17, mainCtrl);
	}
	
	@Override
	public void run() {
		for(Integer[] data : authorId){
			if(data[1] != 0 && data[2] != 0){
				con.saveToDb(data[0],getColumnName(), getPoints());
			}
		}
		
		this.mainCtrl.threadEnded(this.id);
	}
}
