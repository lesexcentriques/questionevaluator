package model.rules;

import ctrl.MainCtrl;

public class Rule3 extends Rule {

	public Rule3(MainCtrl mainCtrl) {
	super(3, "r3", 70, 3, 11, mainCtrl);
	}
	
	@Override
	public void run() {
		for(Integer[] data : authorId){
			if(data[1] == 0 && data[2] != 0){
				con.saveToDb(data[0],getColumnName(), getPoints());
			}
		}
		
		this.mainCtrl.threadEnded(this.id);
	}


}
