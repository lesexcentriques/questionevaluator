package model.rules;

import ctrl.MainCtrl;

public class Rule7 extends Rule {
	
	public Rule7(MainCtrl mainCtrl) {
		super(7, "r7", 50, 4, 1, mainCtrl);
	}
	
	@Override
	public void run() {
		for(Integer[] data : authorId){
			if(data[1] != 0 && data[2] != 0){
				con.saveToDb(data[0],getColumnName(), getPoints());
			}
		}
		
		this.mainCtrl.threadEnded(this.id);
	}

}
