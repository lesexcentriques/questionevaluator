package model.rules;

import ctrl.MainCtrl;

public class Rule2 extends Rule {

	public Rule2(MainCtrl mainCtrl) {
		super(2,"r2", 100 ,4,7, mainCtrl);
	}
	
	@Override
	public void run() {
		for(Integer[] data : authorId){
			if(data[1] != 0 && data[2] != 0){
				con.saveToDb(data[0],getColumnName(), getPoints());
			}
		}
		
		this.mainCtrl.threadEnded(this.id);
	}

}
