package model.rules;

import ctrl.MainCtrl;

public class Rule8 extends Rule {

	public Rule8(MainCtrl mainCtrl) {
		super(8, "r8", 40, 7, 1, mainCtrl);
	}
	
	@Override
	public void run() {
		for(Integer[] data : authorId){
			if(data[1] != 0 && data[2] != 0){
				con.saveToDb(data[0],getColumnName(), getPoints());
			}
		}
		
		this.mainCtrl.threadEnded(this.id);
	}
}
