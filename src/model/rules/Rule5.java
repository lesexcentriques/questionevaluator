package model.rules;

import ctrl.MainCtrl;

public class Rule5 extends Rule {

	public Rule5(MainCtrl mainCtrl) {
		super(5, "r5", 50, 4, 17, mainCtrl);
	}	
	
	@Override
	public void run() {
		for(Integer[] data : authorId){
			if(data[1] != 0 && data[2] != 0){
				con.saveToDb(data[0],getColumnName(), getPoints());
			}
		}
		
		this.mainCtrl.threadEnded(this.id);
	}


}
