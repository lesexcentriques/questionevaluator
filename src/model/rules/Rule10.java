package model.rules;

import ctrl.MainCtrl;

public class Rule10 extends Rule {

	public Rule10(MainCtrl mainCtrl) {
		super(10, "r10", 30, 12, 1, mainCtrl);
	}
	
	@Override
	public void run() {
		for(Integer[] data : authorId){
			if(data[1] != 0 && data[2] != 0){
				con.saveToDb(data[0],getColumnName(), getPoints());
			}
		}
		
		this.mainCtrl.threadEnded(this.id);
	}
}
