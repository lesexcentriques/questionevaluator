package model.rules;

import ctrl.MainCtrl;

public class Rule1 extends Rule {

	public  Rule1(MainCtrl mainCtrl) {
		super(1,"r1", 110 ,4,16, mainCtrl);
	}
	
	@Override
	public void run() {
		for(Integer[] data : authorId){
			if(data[1] != 0 && data[2] != 0){
				con.saveToDb(data[0],getColumnName(), getPoints());
			}
		}
		
		this.mainCtrl.threadEnded(this.id);
	}

}
