package model.rules;

import ctrl.MainCtrl;

public class Rule4 extends Rule {

	public Rule4(MainCtrl mainCtrl) {
		super(4, "r4", 60, 7, 10, mainCtrl);
	}
	
	@Override
	public void run() {
		for(Integer[] data : authorId){
			if(data[1] != 0 && data[2] != 0){
				con.saveToDb(data[0],getColumnName(), getPoints());
			}
		}
		
		this.mainCtrl.threadEnded(this.id);
	}

}
