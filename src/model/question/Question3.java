package model.question;

import java.sql.ResultSet;
import java.sql.SQLException;

import ctrl.DbCtrl;
import ctrl.MainCtrl;

public class Question3 extends Question {

	public Question3(MainCtrl mainCtrl) {
		super(
			3,
			"Mehr als 100 Beitraege",
			20,
			mainCtrl
		);
	}

	@Override
	public void run() {
		String sql = "SELECT author FROM authorall WHERE posts>100";
		
		try {
			ResultSet results = DbCtrl.getInstance().select(sql);
			
			while (results.next()) {
				this.setQuestionPoints(results.getInt("author"));
			}
		} catch (SQLException sqle) {
			logger.error(sqle.toString());
			logger.error(sqle.getMessage());
		}
		
		this.mainCtrl.threadEnded(this.id);
	}

}
