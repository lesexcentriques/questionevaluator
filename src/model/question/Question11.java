package model.question;

import java.sql.ResultSet;
import java.sql.SQLException;

import ctrl.DbCtrl;
import ctrl.MainCtrl;

public class Question11 extends Question {

	public Question11(MainCtrl mainCtrl) {
		super(
			11,
			"Komplette Bewertung mehr als 10 mal wiederholt",
			50,
			mainCtrl
		);
	}

	@Override
	public void run() {
		int author = 0;
		int cnt = 0;
		String sql = "SELECT d.author AS author, (SELECT COUNT(*) FROM datenall WHERE author=d.author GROUP BY text ORDER BY 1 DESC LIMIT 1) AS cnt "
				+ "FROM datenall d "
				+ "JOIN authorall a ON a.author=d.author "
				+ "WHERE a.posts>20 "
				+ "GROUP BY d.author";
		
		try {
			ResultSet results = DbCtrl.getInstance().select(sql);
			
			while (results.next()) {
				author = results.getInt("author");
				cnt = results.getInt("cnt");
				
				if (cnt > 10) {
					this.setQuestionPoints(author);
				}
			}
		} catch (SQLException sqle) {
			logger.error(sqle);
		}
		
		this.mainCtrl.threadEnded(this.id);
	}
}
