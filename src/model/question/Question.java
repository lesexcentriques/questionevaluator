package model.question;

import org.apache.log4j.Logger;

import ctrl.DbCtrl;
import ctrl.MainCtrl;

public abstract class Question implements Runnable {
	final static Logger logger = Logger.getLogger(Question.class);
	//private DbCtrl dbCtrl;
	
	protected int id;
	protected String desc;
	protected int points;
	protected MainCtrl mainCtrl;
	
	// KONSTRUKTOR
	public Question(int id, String desc, int points, MainCtrl mainCtrl) {
		super();
		this.id = id;
		this.desc = desc;
		this.points = points;
		this.mainCtrl = mainCtrl;
		
		//dbCtrl = new DbCtrl();
	}
	
	// FUNKTIONEN
	protected synchronized void setQuestionPoints(int authorId) {
		//logger.info("a" + authorId  + ":q" + this.id + ":p" + this.points);
		DbCtrl.getInstance().saveToDb(authorId, "q" + this.id, this.points);
		//dbCtrl.saveToDb(authorId, "q" + this.id, this.points);
	}
	
	// GETTER
	public int getId() {
		return this.id;
	}
	
	public String getDesc() {
		return this.desc;
	}
}
