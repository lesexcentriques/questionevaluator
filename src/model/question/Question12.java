package model.question;

import java.sql.ResultSet;
import java.sql.SQLException;

import ctrl.DbCtrl;
import ctrl.MainCtrl;

public class Question12 extends Question {

	public Question12(MainCtrl mainCtrl) {
		super(
			12,
			"Rechtschreibfehler pro Bewertung mehr als 10",
			10,
			mainCtrl
		);
	}

	@Override
	public void run() {
		int author = 0;
		double result = 0;
		String sql = "SELECT d.author AS author, count(*)::Float/a.posts AS result "
				+ "FROM mistakes m "
				+ "JOIN datenall d ON d.id=m.id_daten "
				+ "JOIN authorall a ON d.author=a.author "
				+ "GROUP BY d.author, a.posts "
				+ "ORDER BY 1 desc";
		
		try {
			ResultSet results = DbCtrl.getInstance().select(sql);
			
			while (results.next()) {
				author = results.getInt("author");
				result = results.getDouble("result");
				
				if (result > 10) {
					this.setQuestionPoints(author);
				}
			}
		} catch (SQLException sqle) {
			logger.error(sqle.toString());
			logger.error(sqle.getMessage());
		}		
		
		this.mainCtrl.threadEnded(this.id);
	}
}
