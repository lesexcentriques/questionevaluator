package model.question;

import java.sql.ResultSet;
import java.sql.SQLException;

import ctrl.DbCtrl;
import ctrl.MainCtrl;

public class Question1 extends Question {

	public Question1(MainCtrl mainCtrl) {
		super(
			1,
			"Verhaeltnis von Vokabular zur Gesamtanzahl kleiner 0,08",
			20,
			mainCtrl
		);
	}

	@Override
	public void run() {
		String sql = "SELECT author "
				+ "FROM wordsperuser "
				+ "WHERE relation<0.08";
		
		try {
			ResultSet results = DbCtrl.getInstance().select(sql);
			
			while (results.next()) {
				this.setQuestionPoints(results.getInt("author"));
			}
		} catch (SQLException sqle) {
			logger.error(sqle.toString());
			logger.error(sqle.getMessage());
		}
		
		this.mainCtrl.threadEnded(this.id);
	}
	
	

}
