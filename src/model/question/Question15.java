package model.question;

import java.sql.ResultSet;
import java.sql.SQLException;

import ctrl.DbCtrl;
import ctrl.MainCtrl;

public class Question15 extends Question {

	public Question15(MainCtrl mainCtrl) {
		super(
			15,
			"Mehr als 50% nicht verifizierte Bewertungen",
			20,
			mainCtrl
		);
	}

	@Override
	public void run() {
		int author = 0;
		double result = 0.0;
		String sql = "SELECT d.author AS author, 100*count(*)::Float/(SELECT posts FROM authorall a WHERE a.author=d.author) AS result "
				+ "FROM datenall d JOIN authorall a2 ON (a2.author=d.author) "
				+ "WHERE a2.posts>20 "
				+ "AND d.verified='t' "
				+ "GROUP BY d.author";
		
		try {
			ResultSet results = DbCtrl.getInstance().select(sql);
			
			while (results.next()) {
				author = results.getInt("author");
				result = results.getFloat("result");
				
				if (result > 50.0) {
					this.setQuestionPoints(author);
				}
			}
		} catch (SQLException sqle) {
			logger.error(sqle.toString());
			logger.error(sqle.getMessage());
		}
		
		this.mainCtrl.threadEnded(this.id);
	}
}
