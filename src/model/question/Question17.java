package model.question;

import java.sql.ResultSet;
import java.sql.SQLException;

import ctrl.DbCtrl;
import ctrl.MainCtrl;

public class Question17 extends Question {

	public Question17(MainCtrl mainCtrl) {
		super(
			17,
			"Mehr nichthilfreiche als hilfreiche Bewertungen",
			30,
			mainCtrl
		);
	}

	@Override
	public void run() {
		int author = 0;
		int result = 0;
		String sql = "SELECT d.author AS author, SUM(d.counter_helpfull)-SUM(d.counter_all-d.counter_helpfull) AS result "
				+ "FROM datenall d JOIN authorall a2 ON (a2.author=d.author) "
				+ "WHERE a2.posts>20 "
				+ "GROUP BY d.author";
		
		try {
			ResultSet results = DbCtrl.getInstance().select(sql);
			
			while (results.next()) {
				author = results.getInt("author");
				result = results.getInt("result");
				
				if (result < 0) {
					this.setQuestionPoints(author);
				}
			}
		} catch (SQLException sqle) {
			logger.error(sqle.toString());
			logger.error(sqle.getMessage());
		}
		
		this.mainCtrl.threadEnded(this.id);
	}
}
