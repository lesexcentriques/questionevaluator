package model.question;

import java.sql.ResultSet;
import java.sql.SQLException;

import ctrl.DbCtrl;
import ctrl.MainCtrl;

public class Question4 extends Question {

	public Question4(MainCtrl mainCtrl) {
		super(
			4,
			"Mehr als 500 Beitraege",
			30,
			mainCtrl
		);
	}

	@Override
	public void run() {
		String sql = "SELECT author, posts FROM authorall WHERE posts>500";
		
		try {
			ResultSet results = DbCtrl.getInstance().select(sql);
			
			while (results.next()) {
				this.setQuestionPoints(results.getInt("author"));
			}
		} catch (SQLException sqle) {
			logger.error(sqle.toString());
			logger.error(sqle.getMessage());
		}
		
		this.mainCtrl.threadEnded(this.id);
	}
	
}
