package model.question;

import java.sql.ResultSet;
import java.sql.SQLException;

import ctrl.DbCtrl;
import ctrl.MainCtrl;

public class Question13 extends Question {

	public Question13(MainCtrl mainCtrl) {
		super(
			13,
			"Abweichung der Rechtschreibfehler groeßer als 1,5-fache Standardabweichung",
			10,
			mainCtrl
		);
	}

	@Override
	public void run() {
		String sql = "SELECT id_author AS author FROM rating WHERE abnormality>1";
		
		try {
			ResultSet results = DbCtrl.getInstance().select(sql);
			
			while (results.next()) {
				this.setQuestionPoints(results.getInt("author"));
			}
		} catch (SQLException sqle) {
			logger.error(sqle.toString());
			logger.error(sqle.getMessage());
		}	
		
		this.mainCtrl.threadEnded(this.id);
	}
}
