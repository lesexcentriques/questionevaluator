package model.question;

import java.sql.ResultSet;
import java.sql.SQLException;

import ctrl.DbCtrl;
import ctrl.MainCtrl;

public class Question6 extends Question {

	public Question6(MainCtrl mainCtrl) {
		super(
			6,
			"Mehr als 70% 1/5-Sterne-Bewertungen",
			50,
			mainCtrl
		);
	}

	@Override
	public void run() {
		int author = 0;
		double result = 0.0;
		String sql = "SELECT d.author AS author, 100*count(*)::Float/(SELECT posts FROM authorall a WHERE a.author=d.author) AS result "
				+ "FROM datenall d JOIN authorall aa ON (aa.author=d.author) "
				+ "WHERE aa.posts>20 "
				+ "AND (d.rating=1 or d.rating=5) "
				+ "GROUP BY d.author ";
		
		try {
			ResultSet results = DbCtrl.getInstance().select(sql);
			
			while (results.next()) {
				author = results.getInt("author");
				result = results.getFloat("result");
				
				if (result > 70.0) {
					this.setQuestionPoints(author);
				}
			}
		} catch (SQLException sqle) {
			logger.error(sqle.toString());
			logger.error(sqle.getMessage());
		}
		
		this.mainCtrl.threadEnded(this.id);
	}
}
