package model.question;

import java.sql.ResultSet;
import java.sql.SQLException;

import ctrl.DbCtrl;
import ctrl.MainCtrl;

public class Question10 extends Question {

	public Question10(MainCtrl mainCtrl) {
		super(
			10,
			"Drei Saetze mehr als 20 mal wiederholt",
			20,
			mainCtrl
		);
	}

	@Override
	public void run() {
		int author = 0;
		int cnt1 = 0;
		int cnt2 = 0;
		int cnt3 = 0;
		String sql = "SELECT d.author AS author, (SELECT COUNT(*) cnt FROM saetzeall s "
				+ "JOIN datenall r ON s.id_datenall=r.id " 
				+ "WHERE length(s.satz)>15 AND r.author=d.author "
				+ "GROUP BY s.satz ORDER BY 1 DESC limit 1) AS cnt1, "
				+ "(SELECT COUNT(*) cnt FROM saetzeall s "
				+ "JOIN datenall r ON s.id_datenall=r.id "
				+ "WHERE length(s.satz)>15 AND r.author=d.author " 
				+ "GROUP BY s.satz ORDER BY 1 DESC offset 1 limit 1) AS cnt2, "
				+ "(SELECT COUNT(*) cnt FROM saetzeall s "
				+ "JOIN datenall r ON s.id_datenall=r.id "
				+ "WHERE length(s.satz)>15 AND r.author=d.author " 
				+ "GROUP BY s.satz ORDER BY 1 DESC offset 2 limit 1) AS cnt3 "
				+ "FROM datenall d "
				+ "JOIN authorall a ON (d.author=a.author) "
				+ "WHERE a.posts>20 "
				+ "GROUP BY d.author";
		
		try {
			ResultSet results = DbCtrl.getInstance().select(sql);
			
			while (results.next()) {
				author = results.getInt("author");
				cnt1 = results.getInt("cnt1");
				cnt2 = results.getInt("cnt2");
				cnt3 = results.getInt("cnt3");
				
				if (cnt1 > 20 && cnt2 > 20 && cnt3 > 20) {
					this.setQuestionPoints(author);
				}
			}
		} catch (SQLException sqle) {
			logger.error(sqle.toString());
			logger.error(sqle.getMessage());
		}
		
		this.mainCtrl.threadEnded(this.id);
	}
}
