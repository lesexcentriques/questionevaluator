package model.question;

import java.sql.ResultSet;
import java.sql.SQLException;

import ctrl.DbCtrl;
import ctrl.MainCtrl;

public class Question2 extends Question {
	
	public Question2(MainCtrl mainCtrl) {
		super(
			2,
			"Durchschnittliche Anzahl von Woertern pro Bewertung < 25",
			20,
			mainCtrl
		);
	}

	@Override
	public void run() {
		int author = 0;
		double result = 0.0;
		String sql = "SELECT author, cntwords/posts AS result "
				+ "FROM wordsperuser "
				+ "WHERE cntwords/posts>=20.0 "
				+ "ORDER BY 2";
		
		try {
			ResultSet results = DbCtrl.getInstance().select(sql);
			
			while (results.next()) {
				author = results.getInt("author");
				result = results.getInt("result");
				
				if (result < 25.0) {
					this.setQuestionPoints(author);
				}
			}
		} catch (SQLException sqle) {
			logger.error(sqle.toString());
			logger.error(sqle.getMessage());
		}
		
		this.mainCtrl.threadEnded(this.id);
	}
}
