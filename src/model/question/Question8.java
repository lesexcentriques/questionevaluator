package model.question;

import java.sql.ResultSet;
import java.sql.SQLException;

import ctrl.DbCtrl;
import ctrl.MainCtrl;

public class Question8 extends Question {

	public Question8(MainCtrl mainCtrl) {
		super(
			8,
			"Einen Satz mehr als 10 mal wiederholt",
			10,
			mainCtrl
		);
	}
	
	@Override
	public void run() {
		int author = 0;
		int cnt1 = 0;
		String sql = "SELECT d.author AS author, (SELECT COUNT(*) cnt FROM saetzeall s "
				+ "JOIN datenall r ON s.id_datenall=r.id " 
				+ "WHERE length(s.satz)>15 AND r.author=d.author "
				+ "GROUP BY s.satz ORDER BY 1 DESC limit 1) AS cnt1, "
				+ "(SELECT COUNT(*) cnt FROM saetzeall s "
				+ "JOIN datenall r ON s.id_datenall=r.id "
				+ "WHERE length(s.satz)>15 AND r.author=d.author " 
				+ "GROUP BY s.satz ORDER BY 1 DESC offset 1 limit 1) AS cnt2, "
				+ "(SELECT COUNT(*) cnt FROM saetzeall s "
				+ "JOIN datenall r ON s.id_datenall=r.id "
				+ "WHERE length(s.satz)>15 AND r.author=d.author " 
				+ "GROUP BY s.satz ORDER BY 1 DESC offset 2 limit 1) AS cnt3 "
				+ "FROM datenall d "
				+ "JOIN authorall a ON (d.author=a.author) "
				+ "WHERE a.posts>20 "
				+ "GROUP BY d.author";
		
		try {
			ResultSet results = DbCtrl.getInstance().select(sql);
			
			while (results.next()) {
				author = results.getInt("author");
				cnt1 = results.getInt("cnt1");
				
				if (cnt1 > 10) {
					this.setQuestionPoints(author);
				}
			}
		} catch (SQLException sqle) {
			logger.error(sqle.toString());
			logger.error(sqle.getMessage());
		}
		
		this.mainCtrl.threadEnded(this.id);
	}
}
